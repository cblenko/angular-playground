import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserNameCheckValidator } from './validators/username-check.directive';
import { PasswordCheckValidator } from './validators/password-check.directive';

import { LoginFormComponent } from './login-form.component/login-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UserNameCheckValidator,
    PasswordCheckValidator
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
