import { AbstractControl, ValidatorFn } from '@angular/forms'
import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';


@Directive({
    selector: '[username-check][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: UserNameCheckValidator, multi: true }
    ]
})
export class UserNameCheckValidator implements Validator {
    validator: ValidatorFn;
    
    constructor() {
      this.validator = usernameCheckFactory();
    }
    
    validate(c: FormControl) {
      return this.validator(c);
    }
}



function usernameCheckFactory() : ValidatorFn {
    return (c: AbstractControl) => {
        /* this needs to return either null or
        {
            <validatorname>: {
                valid: true / false
            }
        }
        */
        if((c.value || '').match(/@blenko\.co\.uk/i)) {
            return null
        } else {
            return {
                usernamecheck: {
                    valid: false
                }
            }
        }

    }
}