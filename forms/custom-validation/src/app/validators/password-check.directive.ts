import { AbstractControl, ValidatorFn } from '@angular/forms'
import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';


@Directive({
    selector: '[password-check][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: PasswordCheckValidator, multi: true }
    ]
})
export class PasswordCheckValidator implements Validator {
    validator: ValidatorFn;
    
    constructor() {
      this.validator = passwordCheckFactory();
    }
    
    validate(c: FormControl) {
      return this.validator(c);
    }
}


// remote commentary added
function passwordCheckFactory() : ValidatorFn {
    return (c: AbstractControl) => {
        /* this needs to return either null or
        {
            <validatorname>: {
                valid: true / false
            }
        }
        */
        if((c.value || '').match(/password/)) {
            return null
        } else {
            return {
                passwordcheck: {
                    valid: false
                }
            }
        }

    }
}