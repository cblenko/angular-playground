import { Component, ContentChildren, ViewChild, QueryList, ElementRef, AfterContentInit, AfterViewInit } from '@angular/core';
import { WizardStepComponent } from './wizard-step.component/wizard-step.component';

@Component({
    selector: 'wizard',
    styleUrls: ['./wizard.component.scss'],
    template: `
        <div class="wizard-holder">
            <ul>
                <li *ngFor="let step of initialSteps" [ngClass]="{ 'active': step.active }">{{step.label}}</li>
            </ul>
            <div class="wizard-content">
                <ng-content></ng-content>
            </div>
            <div class="wizard-footer" #footer>
                <button class="prev" [hidden]="!hasPrev" (click)="prev()">Prev</button>
                <button class="next" [hidden]="!hasNext" (click)="next()">Next</button>
            </div>
        </div>
    `
})
export class WizardComponent implements AfterContentInit, AfterViewInit {
    // VIEWCHILDREN are elements in the template
    // CONTENTCHILDREN are elements within the component tags - elements that are likely to be projected

    @ContentChildren(WizardStepComponent) initialSteps : QueryList<WizardStepComponent>;
    @ViewChild("footer") footer : ElementRef;
    
    private _steps: WizardStepComponent[] = [];
    private _activeStep: WizardStepComponent;
    private _activeStepIndex = 0;

    constructor() {}

    ngAfterViewInit() {
        this.footer.nativeElement.style.backgroundColor = '#343454';
    }

    ngAfterContentInit() {
        this.initialSteps.forEach(element => {
            this._steps.push(element);
        });
        
        this.setActiveStep(this._steps[this._activeStepIndex]);
    }

    setActiveStep(step: WizardStepComponent) {
        if(this._activeStep)
            this._activeStep.active = false;

        step.active = true;
        this._activeStep = step;
    }

    get hasPrev(): boolean {
        return this._activeStepIndex > 0;
    }

    get hasNext(): boolean {
        return this._activeStepIndex + 1 < this._steps.length;
    }

    prev() {
        if(!this.hasPrev) return;

        this.setActiveStep(this._steps[--this._activeStepIndex]);
    }

    next() {
        if(!this.hasNext) return;
        
        this.setActiveStep(this._steps[++this._activeStepIndex]);
    }

}
