import { Component, Input, OnInit } from '@angular/core'

@Component({
    selector: 'wizard-step',
    styleUrls: ['./wizard-step.component.scss'],
    template: `
        <div [ngClass]="{'hidden': !active}">
            <ng-content></ng-content>
        </div>
    `
})
export class WizardStepComponent implements OnInit {
    @Input() label: string;
    active: boolean = false;

    constructor() {}

    ngOnInit() {
    }
}