import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/of';
import 'rxjs/add/operator/delay';

import { Competition } from '../models/competition.interface';

@Injectable()
export class CompetitionsService {
  comps = [
    { Id: 1, Name: "English Premier League 2017/18", CountryCode: 'GB' },
    { Id: 2, Name: "Championship 2017/18", CountryCode: 'GB' },
    { Id: 3, Name: "League One 2017/18", CountryCode: 'GB' }
  ]

  constructor() { }

  getCompetitions(): Observable<Competition[]> {
    return Observable.of(this.comps).delay(1000);
  }

  getCompetition(id): Competition {
    return this.comps.find( (comp) => comp.Id === +id);
  }

}
