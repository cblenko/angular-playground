export interface Competition {
    Id: number,
    Name: string,
    CountryCode: string
}