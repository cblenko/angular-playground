import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Route, Router, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { LastManStandingModule } from './last-man-standing/last-man-standing.module';
import { LastManStandingComponent } from './last-man-standing/last-man-standing.component/last-man-standing.component';
import { CompetitionListComponent } from './last-man-standing//competition-list/competition-list.component';
import { NotFoundComponent } from './ancillary/not-found/not-found.component';

export const ROUTES: Routes = [
  /*{
    path: '',
    redirectTo: '/lastmanstanding',
    pathMatch: 'full'
  },
  { path: '**', component: NotFoundComponent }  */
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    LastManStandingModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private router: Router) {
    console.log(router);
  }
}
