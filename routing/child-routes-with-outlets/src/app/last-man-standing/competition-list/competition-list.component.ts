import { Component, OnInit } from '@angular/core';
import { CompetitionsService } from '../../services/competitions.service';
import { Competition } from '../../models/competition.interface';

import 'rxjs/operator/map';

@Component({
  selector: 'competition-list',
  templateUrl: './competition-list.component.html',
  styleUrls: ['./competition-list.component.scss']
})
export class CompetitionListComponent implements OnInit {
  Competitions: Competition[];

  constructor(private CompetitionServices: CompetitionsService) {
    
   }

  ngOnInit() {
    this.CompetitionServices.getCompetitions()
      .subscribe( (competitions) => {
        this.Competitions = competitions;
      })
  }

}
