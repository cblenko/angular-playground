import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Competition } from '../../models/competition.interface';
import { CompetitionsService } from '../../services/competitions.service';
import { Observable } from 'rxjs/Observable'

@Component({
  selector: 'competition-detail',
  templateUrl: './competition-detail.component.html',
  styleUrls: ['./competition-detail.component.scss']
})
export class CompetitionDetailComponent implements OnInit, OnDestroy {
  Comp: Competition;
  private Params: any;


  constructor(private CurrentRoute: ActivatedRoute, private CompetitionServices: CompetitionsService) { }

  ngOnInit() {
    this.Params = this.CurrentRoute.params.subscribe( (params) => {
      this.Comp = this.CompetitionServices.getCompetition(params.id);
    })
  }

  ngOnDestroy() {
        this.Params.unsubscribe();
  }

}
