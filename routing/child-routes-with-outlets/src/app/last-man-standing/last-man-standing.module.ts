import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router'
import { CommonModule } from '@angular/common';
import { LastManStandingComponent } from './last-man-standing.component/last-man-standing.component';
import { CompetitionListComponent } from './competition-list/competition-list.component';

import { CompetitionsService } from '../services/competitions.service';
import { CompetitionDetailComponent } from './competition-detail/competition-detail.component';

const ROUTES: Route[] = [
  { 
    path: 'lastmanstanding', 
    component: LastManStandingComponent,
    children: [
      { 
        path: 'competitions',
        component: CompetitionListComponent
      },
      { 
        path: 'competitions/:id',
        component: CompetitionDetailComponent,
        outlet: 'lms'
      }
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  providers: [CompetitionsService],
  declarations: [LastManStandingComponent, CompetitionListComponent, CompetitionDetailComponent]
})
export class LastManStandingModule { }
