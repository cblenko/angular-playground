import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamViewerComponent } from './team-viewer/team-viewer.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TeamListComponent, TeamViewerComponent]
})
export class TeamDashboardModule { }
