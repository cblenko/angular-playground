import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TeamDashboardModule } from './team-dashboard/team-dashboard.module';
import { HomeModule } from './home/home.module';
import { RouterModule, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home/home.component';
import { TeamListComponent } from './team-dashboard/team-list/team-list.component';
import { TeamViewerComponent } from './team-dashboard/team-viewer/team-viewer.component';

const BaseRoutes : Route[] = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'teamlist', component: TeamListComponent, pathMatch: 'full' },
  { path: 'teamviewer', component: TeamViewerComponent, pathMatch: 'full' }
]


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(BaseRoutes),
    TeamDashboardModule,
    HomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
