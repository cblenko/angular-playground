import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/of';
import 'rxjs/add/operator/delay';

import { Team } from '../models/team.interface';

@Injectable()
export class TeamService {
  getTeams() : Observable<Team[]> {
    let teams: Team[] = [
      { id: 1, name: 'Manchester United', active: true },
      { id: 2, name: 'Manchester City', active: true },
      { id: 3, name: 'Arsenal', active: true },
      { id: 4, name: 'Leeds United', active: true },
      { id: 5, name: 'West Ham', active: true },
      { id: 6, name: 'Fulham', active: true },
      { id: 7, name: 'Bradford City', active: true },
    ]

    return Observable.of(teams).delay(1000);
  }

  constructor() { }

}
