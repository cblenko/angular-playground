import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { UIRouterModule } from '@uirouter/angular';

import { AppComponent } from './app.component';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamViewerComponent } from './team-viewer/team-viewer.component';
import { TeamService } from './services/team.service';

let states = [
  { name: 'Teams', url: '/teams', component: TeamListComponent },
  { name: 'Teams.Detail',
    url: '/teams/:teamId',
    component: TeamViewerComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    TeamListComponent,
    TeamViewerComponent
  ],
  imports: [
    BrowserModule,
    UIRouterModule.forRoot({ states: states, useHash: true })
  ],
  providers: [TeamService],
  bootstrap: [AppComponent]
})
export class AppModule { }
