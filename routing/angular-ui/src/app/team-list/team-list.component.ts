import { Component, OnInit, Injectable } from '@angular/core';

import { TeamService } from '../services/team.service';
import { Team } from '../models/team.interface';

@Component({
  selector: 'team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
@Injectable()
export class TeamListComponent implements OnInit {
  teams: Team[] = []

  constructor(private t: TeamService) { 
    t.getTeams()
      .subscribe((TeamData) => {
        this.teams = TeamData;
      })
  }

  ngOnInit() {
  }

}