import { Component, OnInit, Input, Injectable } from '@angular/core';
import { Transition } from '@uirouter/angular'

import { Team } from '../models/team.interface';

@Component({
  selector: 'team-viewer',
  templateUrl: './team-viewer.component.html',
  styleUrls: ['./team-viewer.component.scss']
})
@Injectable()
export class TeamViewerComponent implements OnInit {
  teamId: number;

  constructor(private trans: Transition) {
    this.teamId = trans.params().teamId;
  }

  ngOnInit() {
  }

}
