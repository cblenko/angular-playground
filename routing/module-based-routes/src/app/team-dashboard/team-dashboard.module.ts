import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Route } from '@angular/router'

import { TeamListComponent } from './team-list/team-list.component';
import { TeamViewerComponent } from './team-viewer/team-viewer.component';

const TeamRoutes: Route[] = [
  { path: 'teamlist', component: TeamListComponent, pathMatch: 'full' },
  { path: 'teamviewer', component: TeamViewerComponent, pathMatch: 'full' }
]


@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    TeamListComponent, TeamViewerComponent  // as we are using these only within above routes, they do not technically need to be exported
  ],
  declarations: [TeamListComponent, TeamViewerComponent]
})
export class TeamDashboardModule { }
