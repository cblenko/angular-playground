import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PopoverComponent } from './popover/popover.component';

@NgModule({
  declarations: [
    AppComponent,
    PopoverComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [PopoverComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
