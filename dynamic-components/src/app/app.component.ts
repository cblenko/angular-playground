import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, AfterContentInit } from '@angular/core';

import { PopoverComponent } from './popover/popover.component'

@Component({
  selector: 'app-root',
  template: `
    <button (click)="showPopover()">Popover</button>
    <ng-template #popover></div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterContentInit {
  @ViewChild('popover', { read: ViewContainerRef }) popover: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver){}

  showPopover() {
    const popoverFactory = this.resolver.resolveComponentFactory(PopoverComponent);
    const comp = this.popover.createComponent(popoverFactory)

    var pos = {
      left: this.popover.element.nativeElement.offsetLeft + this.popover.element.nativeElement.offsetWidth,
      top: this.popover.element.nativeElement.offsetTop
    }

    console.log(this.popover.element)

    comp.location.nativeElement.children[0].style.top = pos.top;
    comp.location.nativeElement.children[0].style.left = pos.left;
    
  }

  ngAfterContentInit() {}
}
